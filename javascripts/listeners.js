const os = require("os");

document
  .getElementById("content")
  .addEventListener("keydown", ({ ctrlKey, metaKey, key }) => {
    const platform = os.platform();

    const isPressingSaveKey = platform === "darwin" ? metaKey : ctrlKey;
    if (isPressingSaveKey && key === "s") {
      document.getElementById("save-button").click();
    }
  });
