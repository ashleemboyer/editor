const { getCurrentWindow, dialog } = require("electron").remote;
const fs = require("fs");

const contentElement = document.getElementById("content");
contentElement.focus();

const getAsMarkdown = elements => {
  let firstLine;
  let toIterate;

  if (!elements) {
    const innerHTML = contentElement.innerHTML;
    const indexOfDiv = innerHTML.indexOf("<div>");

    if (indexOfDiv !== -1) {
      firstLine = innerHTML.substring(0, indexOfDiv) + "\n";
    } else {
      firstLine = innerHTML + "\n";
    }

    toIterate = contentElement.children;
  } else {
    firstLine = "";
    toIterate = elements;
  }

  let markdown = firstLine;
  Array.from(toIterate).forEach(el => {
    if (el.children.length > 0) {
      markdown += getAsMarkdown(el) + "\n";
    } else if (el.innerHTML === "<br>") {
      markdown += "\n";
    } else {
      markdown += el.innerHTML + "\n";
    }
  });

  return markdown;
};

document.getElementById("save-button").addEventListener("click", () => {
  dialog
    .showSaveDialog({ properties: ["openFile", "multiSelections"] })
    .then(({ filePath }) => {
      if (filePath) {
        fs.writeFile(filePath, getAsMarkdown(), err => {
          if (err) {
            throw err;
          }
        });
      }
    });
});

document.getElementById("close-button").addEventListener("click", () => {
  const window = getCurrentWindow();
  window.close();
});
